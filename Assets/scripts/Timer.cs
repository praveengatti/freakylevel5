﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Khadga.FreakSpace;

namespace Lean.Touch
{
    public class Timer : MonoBehaviour
    {
        public float toughness = 0.2f;
        public static float time;
        public Text timetext;
        public Slider TimeSlider;
        private static Timer instance;

        // Game Instance Singleton
        public static Timer Instance
        {
            get
            {
                return instance;
            }
        }

        public void Awake()
        {
            Time.timeScale = 0;
            if (instance != null && instance != this)
            {
                //Destroy(this.gameObject);
            }

            instance = this;
             //DontDestroyOnLoad(this.gameObject);
        }
        // Use this for initialization
        void Start()
        {
            TimeSlider.maxValue = 100;
            TimeSlider.minValue = 0;
        }

     
        public void leave()
        {
             RandomTextGenerator.Instance.InputMessage();
             if(Levelmanager.Instance.Level_Index==4)
             {
                 RandomTextGenerator.Instance.Inputmessage2();
                 RandomTextGenerator.Instance.Inputmessage3();
             }
             time += 1;
             Time.timeScale = 1;
            if(Levelmanager.Instance.Level_Index == 3)
             SPAWNER.Instance.spawn();
        }

        public void stop()
        {
            Time.timeScale = 0;
            time = 0;
            RandomTextGenerator.Instance.praveen = false;
           // gameover.enabled = true;
            RandomTextGenerator.Instance.inputText.enabled = false;
            RandomTextGenerator.Instance.inputText2.enabled = false;
            RandomTextGenerator.Instance.inputText3.enabled = false;
            RandomTextGenerator.Instance.GameOver_panel.SetActive(true);
        }

      void ColouCase(Color clr)
        {
                if (SPAWNER.Instance.up.color != clr&&
                    SPAWNER.Instance.down.color != clr &&
                    SPAWNER.Instance.left.color != clr &&
                    SPAWNER.Instance.right.color != clr)
                {
                    leave();
                }
                else
                    stop();
        }

        void NotColorCase(Color clr)
        {
            if (SPAWNER.Instance.up.color == clr &&
                       SPAWNER.Instance.up.color == clr &&
                       SPAWNER.Instance.up.color == clr &&
                       SPAWNER.Instance.up.color == clr)
            {
                leave();
            }
            else
                stop();
        }

        // Update is called once per frame
        void Update()
        {
            if (Levelmanager.Instance.Level_Index == 5)
            {
                if (LeanSwipeDirection4.Instance.left1 == true &&
                   LeanSwipeDirection4.Instance.right1 == true)
                {
                    SoundManager.Instance.PlaySound("swipe");
                    RandomTextGenerator.Instance.InputMessage();
                    RandomTextGenerator.Instance.Inputmessage2();
                    RandomTextGenerator.Instance.inputText.enabled = true;
                    RandomTextGenerator.Instance.inputText2.enabled = true;
                    LeanSwipeDirection4.Instance.left1 = false;
                    LeanSwipeDirection4.Instance.right1 = false;
                    ScoreManager.scorevalue += 1;
                    Timer.time += 1;
                }

                if (RandomTextGenerator.Instance.inputText.text == "Nothing" ||
                    RandomTextGenerator.Instance.inputText.text == "NotNotNothing")
                {
                    LeanSwipeDirection4.Instance.left1 = true;
                }
                if (RandomTextGenerator.Instance.inputText2.text == "Nothing" ||
                   RandomTextGenerator.Instance.inputText2.text == "NotNotNothing")
                {
                    LeanSwipeDirection4.Instance.right1 = true;
                }
            }

            if (time > 1)
            {
                time = 1;
            }

            else if (time > 0)
            {
                TimeSlider.value = time * 100;
                // if (ScoreManager.ScoreValue <= 25)
                // time -= Time.deltaTime * (ScoreManager.ScoreValue * toughness);
                // else
                time -= Time.deltaTime * 0.6f;
                timetext.text = "TimeLeft: " + time;
            }


            else if (time < 0)
            {
                if (Levelmanager.Instance.Level_Index != 5 &&
                    Levelmanager.Instance.Level_Index != 3)
                {
                    if (RandomTextGenerator.Instance.inputText.text == "Nothing" ||
                      RandomTextGenerator.Instance.inputText.text == "NotNotNothing" ||
                      RandomTextGenerator.Instance.inputText2.text == "Nothing" ||
                      RandomTextGenerator.Instance.inputText2.text == "NotNotNothing" ||
                      RandomTextGenerator.Instance.inputText3.text == "Nothing" ||
                      RandomTextGenerator.Instance.inputText3.text == "NotNotNothing")
                    {
                        leave();
                    }
                    else
                        stop();
                }

                else if (RandomTextGenerator.Instance.inputText.text == "RED")
                {
                    ColouCase(Color.red);
                }
                else if (RandomTextGenerator.Instance.inputText.text == "YELLOW")
                {
                    ColouCase(Color.yellow);
                }
                else if (RandomTextGenerator.Instance.inputText.text == "BLUE")
                {
                    ColouCase(Color.blue);
                }
                else if (RandomTextGenerator.Instance.inputText.text == "GREEN")
                {
                    ColouCase(Color.green);
                }

                else if (RandomTextGenerator.Instance.inputText.text == "NotRED")
                {
                    NotColorCase(Color.red);
                }
                else if (RandomTextGenerator.Instance.inputText.text == "NotGREEN")
                {
                    NotColorCase(Color.green);
                }

                else if (RandomTextGenerator.Instance.inputText.text == "NotBLUE")
                {
                    NotColorCase(Color.blue);
                }
                else if (RandomTextGenerator.Instance.inputText.text == "NotYELLOW")
                {
                    NotColorCase(Color.yellow);
                }

                else if (Levelmanager.Instance.Level_Index == 5)
                {
                    if (RandomTextGenerator.Instance.inputText.text == "Nothing")
                    {
                        RandomTextGenerator.Instance.InputMessage();
                        Time.timeScale = 1;
                    }
                    else if (RandomTextGenerator.Instance.inputText2.text == "Nothing")
                    {

                        RandomTextGenerator.Instance.Inputmessage2();
                        Time.timeScale = 1;
                    }
                    else
                        stop();
                }

                else
                {
                    stop();
                }
            }
        }
    }
}
