﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Lean.Touch
{

    public class Freaky : MonoBehaviour
    {

    
        private static Freaky instance;
        // Game Instance Singleton
        public static Freaky Instance
        {
            get
            {
                return instance;
            }
        }

        public void Awake()
        {
            Time.timeScale = 0;
            if (instance != null && instance != this)
            {
                // Destroy(this.gameObject);
            }

            instance = this;
            //DontDestroyOnLoad(this.gameObject);
        }
        // Use this for initialization
        void Start()
        {
          
        }

        // Update is called once per frame
        void Update()
        {

        }

    }
}
