﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Khadga.FreakSpace;
namespace Lean.Touch
{
    public class Level2 : LeanSwipeDirection4
    {
        public override void Start()
        {
            RandomTextGenerator.Instance.starting();
        }
        public override void Success()
        {
            SoundManager.Instance.PlaySound("swipe");
            RandomTextGenerator.Instance.InputMessage();
            Time.timeScale = 1;
            Timer.time += 1;
            ScoreManager.scorevalue += 1;
        }

      

        public override void OnFingerSwipe(LeanFinger finger)
        {
            // Make sure the info text exists
            // if (Text2 != null)
            // {
            // Store the swipe delta in a temp variable
            var swipe = finger.SwipeScreenDelta;

            if (RandomTextGenerator.Instance.praveen == true)
            {
                if (swipe.x < -Mathf.Abs(swipe.y))
                {
                    Debug.Log("Left");
                    if (RandomTextGenerator.Instance.inputText.text == "Left"
                                          || RandomTextGenerator.Instance.inputText.text == "NotNotLeft"
                                        || RandomTextGenerator.Instance.inputText.text == "NotNothing"
                                       || RandomTextGenerator.Instance.inputText.text == "NotRight"
                                         || RandomTextGenerator.Instance.inputText.text == "NotUp"
                                        || RandomTextGenerator.Instance.inputText.text == "NotDown")
                    {
                        Success();
                    }
                    else
                        Fail();
                }
            }

            if (RandomTextGenerator.Instance.praveen == true)
            {

                if (swipe.x > Mathf.Abs(swipe.y))
                {
                    Debug.Log("right");
                    if (RandomTextGenerator.Instance.inputText.text == "Right"
                                       || RandomTextGenerator.Instance.inputText.text == "NotNotRight"
                                       || RandomTextGenerator.Instance.inputText.text == "NotLeft"
                                       || RandomTextGenerator.Instance.inputText.text == "NotUp"
                                       || RandomTextGenerator.Instance.inputText.text == "NotDown"
                                       || RandomTextGenerator.Instance.inputText.text == "NotNothing"
                                       )
                    {
                        Success();
                    }
                    else
                        Fail();
                }
            }

            if (RandomTextGenerator.Instance.praveen == true)
            {

                if (swipe.y < -Mathf.Abs(swipe.x))
                {
                    Debug.Log("down");

                    if (RandomTextGenerator.Instance.inputText.text == "Down"
                                   || RandomTextGenerator.Instance.inputText.text == "NotNotDown"
                                   || RandomTextGenerator.Instance.inputText.text == "NotNothing"
                                   || RandomTextGenerator.Instance.inputText.text == "NotLeft"
                                   || RandomTextGenerator.Instance.inputText.text == "NotRight"
                                   || RandomTextGenerator.Instance.inputText.text == "NotUp"
                                  )
                    {
                        Success();
                    }
                    else
                        Fail();
                }
            }

            if (RandomTextGenerator.Instance.praveen == true)
            {

                if (swipe.y > Mathf.Abs(swipe.x))
                {


                    if (RandomTextGenerator.Instance.inputText.text == "Up"
                  || RandomTextGenerator.Instance.inputText.text == "SWIPE UP"
                   || RandomTextGenerator.Instance.inputText.text == "NotNotUp"
                   || RandomTextGenerator.Instance.inputText.text == "NotNothing"
                  || RandomTextGenerator.Instance.inputText.text == "NotRight"
                   || RandomTextGenerator.Instance.inputText.text == "NotLeft"
                   || RandomTextGenerator.Instance.inputText.text == "NotDown"
                 )
                    {
                        Success();

                    }
                    else
                        Fail();
                }


            }
        }
    }
}