﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Lean.Touch
{
    public class SPAWNER : MonoBehaviour
    {
      //  public float Alpha = 125;
        private static SPAWNER instance;
        public int abcd;
        public Image left,right,up,down;
        public Color[] colours = new Color[4];


        // Game Instance Singleton
        public static SPAWNER Instance
        {
            get
            {
                return instance;
            }
        }

        public void Awake()
        {
            Time.timeScale = 0;
            if (instance != null && instance != this)
            {
                //Destroy(this.gameObject);
            }

            instance = this;
            // DontDestroyOnLoad(this.gameObject);
        }

        // Use this for initialization
        void Start()
        {
            colours[0] = Color.blue;
            colours[1] = Color.red;
            colours[2] = Color.yellow;
            colours[3] = Color.green;
        }
       

        // Update is called once per frame
        void Update()
        {
           
        }

        public void spawn()
        {

             up.color = colours[ Random.Range(0, 4)];

             left.color = colours[Random.Range(0, 4)];

             right.color = colours[Random.Range(0, 4)];

             down.color = colours[Random.Range(0, 4)];

        }
           
        }
    }



