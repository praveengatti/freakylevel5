﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using System.Collections;

namespace Lean.Touch
{

    // This script will tell you which direction you swiped in
    public class SplitScreen : MonoBehaviour
    {
        public GameObject playbutton;
        public Text inputText;

        public void Start()
        {
          
        }

       public void gameplay()
        {
            Time.timeScale = 1;
            playbutton.SetActive(false);
            RandomTextGenerator.Instance.InputMessage();
        }
        public void Inputmessage()
        {
            Commands[] validCommands = new[] { Commands.Up, Commands.Right, Commands.Left,
            Commands.Down,Commands.Nothing};


            Commands inputCommand = validCommands[Random.Range(0, validCommands.Length - 14)];
            Debug.Log(inputCommand);
            inputText.text = inputCommand.ToString();
        }
        protected virtual void OnEnable()
        {

            // Hook into the events we need
            LeanTouch.OnFingerSwipe += OnFingerSwipe;

        }

        protected virtual void OnDisable()
        {
            // Unhook the events
            LeanTouch.OnFingerSwipe -= OnFingerSwipe;
        }


        public void OnFingerSwipe(LeanFinger finger)
        {
            // Make sure the info text exists
            // if (Text2 != null)
            // {
            // Store the swipe delta in a temp variable
            var swipe = finger.SwipeScreenDelta;

            
                if (swipe.x < -Mathf.Abs(swipe.y))
                {
                    Debug.Log("left");
                }

                if (swipe.x > Mathf.Abs(swipe.y))
                {
                    Debug.Log("right");

                }

          

                if (swipe.y < -Mathf.Abs(swipe.x))
                {
                    Debug.Log("down");

                }

          

                if (swipe.y > Mathf.Abs(swipe.x))
                {
                    Debug.Log("up");


                }
        }
    }
}



