﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Khadga.FreakSpace;

namespace Lean.Touch
{
    public class ScoreManager : MonoBehaviour
    {
        public static int scorevalue;
        public Text Highscore;
        public Text yourscore;
        public Text Score;
        private static ScoreManager instance;

        // Game Instance Singleton
        public static ScoreManager Instance
        {
            get
            {
                return instance;
            }
        }

        public void Awake()
        {
            Time.timeScale = 0;
            if (instance != null && instance != this)
            {
                // Destroy(this.gameObject);
            }

            instance = this;
          // DontDestroyOnLoad(this.gameObject);
        }

        // Use this for initialization
        void Start()
        {
         
        }

        // Update is called once per frame
        void Update()
        {
            Score.text = "Score : " + scorevalue;
            if (Levelmanager.Instance.Level_Index == 1)
              Highscore.text = "HIGH SCORE: " + PlayerManager.Instance.GetCurrency("Level1");
            else if (Levelmanager.Instance.Level_Index == 2)
               Highscore.text = "HIGH SCORE: " + PlayerManager.Instance.GetCurrency("Level2");
            else if (Levelmanager.Instance.Level_Index == 3)
               Highscore.text = "HIGH SCORE: " + PlayerManager.Instance.GetCurrency("Level3");
            else if (Levelmanager.Instance.Level_Index == 4)
               Highscore.text = "HIGH SCORE: " + PlayerManager.Instance.GetCurrency("Level4");
            else if (Levelmanager.Instance.Level_Index == 5)
               Highscore.text = "HIGH SCORE: " + PlayerManager.Instance.GetCurrency("Level5");

            yourscore.text = "YOUR SCORE: " + ScoreManager.scorevalue ;
        }
    }
}
