﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Khadga.FreakSpace;

namespace Lean.Touch
{

    public class Levelmanager : MonoBehaviour
    {
        public Button LEVEL1b, LEVEL2b, LEVEL3b, LEVEL4b, LEVEL5b;
        public Image l2,l3,l4,l5;
        public int Level_Index;
        private static Levelmanager instance;
     

        // Game Instance Singleton
        public static Levelmanager Instance
        {
            get
            {
                return instance;
            }
        }

        public void Awake()
        {
            Time.timeScale = 0;
            if (instance != null && instance != this)
            {
                // Destroy(this.gameObject);
            }
            instance = this;
            DontDestroyOnLoad(this.gameObject);
        }

        // Use this for initialization
        void Start()
        {
            Initialize();
        }

        void Initialize()
        {
            LEVEL2b.interactable = false;
            LEVEL3b.interactable = false;
            LEVEL4b.interactable = false;
            LEVEL5b.interactable = false;

            l2.enabled = true;
            l3.enabled = true;
            l4.enabled = true;
            l5.enabled = true;

            if (PlayerManager.Instance.GetCurrency("Level1") >= 30)
            {
                Check_Level(LEVEL2b, l2);
            }
            if (PlayerManager.Instance.GetCurrency("Level2") >= 30)
            {
                Check_Level(LEVEL3b, l3);
            }
            if (PlayerManager.Instance.GetCurrency("Level3") >= 40)
            {
                Check_Level(LEVEL4b, l4);
            }
            if (PlayerManager.Instance.GetCurrency("Level4") >= 40)
            {
                Check_Level(LEVEL5b, l5);
            }
        }

        void Check_Level(Button b,Image i)
        {
            b.interactable = true;
            i.enabled = false;
        }

        // Update is called once per frame
        void Update()
        {

        }

        public void Set_LevelIndexI(int x)
        {
            Level_Index = x;
        }

        public void LoadLevel(string lvl)
        {
            SceneManager.LoadScene(lvl);
            ScoreManager.scorevalue = 0;
        }

        public void RotateScreen()
        {
            Screen.orientation = ScreenOrientation.LandscapeLeft;
        }
       
        public void back()
        {
            SceneManager.LoadScene("START");
        }
    }

}