﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Khadga.FreakSpace;

namespace Lean.Touch
{
    public class Level1 : LeanSwipeDirection4
    {

        public override void Start()
        {
            RandomTextGenerator.Instance.starting();
        }

        public override void Success()
        {
            SoundManager.Instance.PlaySound("swipe");
            RandomTextGenerator.Instance.InputMessage();
            Time.timeScale = 1;
            Timer.time += 1;
            ScoreManager.scorevalue += 1;
        }

      

        public override void OnFingerSwipe(LeanFinger finger)
        {
            // Make sure the info text exists
            // if (Text2 != null)
            // {
            // Store the swipe delta in a temp variable
            var swipe = finger.SwipeScreenDelta;

            if (RandomTextGenerator.Instance.praveen == true)
            {
                if (swipe.x < -Mathf.Abs(swipe.y))
                {
                    Debug.Log("Left");
                    if (RandomTextGenerator.Instance.inputText.text == "Left")
                    {
                        Success();
                    }
                    else
                        Fail();
                }
            }

            if (RandomTextGenerator.Instance.praveen == true)
            {

                if (swipe.x > Mathf.Abs(swipe.y))
                {
                    Debug.Log("right");
                    if (RandomTextGenerator.Instance.inputText.text == "Right")
       
                    {
                        Success();
                    }
                    else
                        Fail();
                }
            }

            if (RandomTextGenerator.Instance.praveen == true)
            {

                if (swipe.y < -Mathf.Abs(swipe.x))
                {
                    Debug.Log("down");

                    if (RandomTextGenerator.Instance.inputText.text == "Down")
                                  
                    {
                        Success();
                    }
                    else
                        Fail();
                }
            }

            if (RandomTextGenerator.Instance.praveen == true)
            {

                if (swipe.y > Mathf.Abs(swipe.x))
                {
                    

                  if (RandomTextGenerator.Instance.inputText.text == "Up"
                || RandomTextGenerator.Instance.inputText.text == "SWIPE UP")
                    {
                        Success();
                    }
                    else
                        Fail();
                }
                

            }
        }
    }
}