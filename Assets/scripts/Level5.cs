﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using System.Collections;
using Khadga.FreakSpace;

namespace Lean.Touch
{

    // This script will tell you which direction you swiped in
    public class Level5 : LeanSwipeDirection4
    {
        public GameObject leftbutton;
        public GameObject rightbutton;
        public bool leftb = false;
        public bool rightb = false;

        //  [Tooltip("The text element we will display the swipe information in")]

        public void left()
        {
            leftb = true;
            rightb = false;
        }
        public void right()
        {
            rightb = true;
            leftb = false;
        }

        public override void Start()
        {
            RandomTextGenerator.Instance.starting();
            leftbutton.SetActive(true);
            rightbutton.SetActive(true);
        }

        public void Leftfun()
        {
            RandomTextGenerator.Instance.inputText.enabled = false;
            leftb = false;
            left1 = true;
        }

        public void Rightfun()
        {
            RandomTextGenerator.Instance.inputText2.enabled = false;
            rightb = false;
            right1 = true;
        }

        public void lefttext(Text texts,string str1,string str2,string str3,string str4,string str5,string str6)
        {
            if ( texts.text == str1)
                Leftfun();
            else if (texts.text == str2)
                Leftfun();
            else if (texts.text == str3)
                Leftfun();
            else if (texts.text == str4)
                Leftfun();
            else if (texts.text ==str5)
                Leftfun();
            else if (texts.text == str6)
                Leftfun();
            else
                Fail();
        }
        public void righttext(Text texts, string str1, string str2, string str3, string str4, string str5, string str6)
        {
            if (texts.text == str1)
                Rightfun();
            else if (texts.text == str2)
                Rightfun();
            else if (texts.text == str3)
                Rightfun();
            else if (texts.text == str4)
                Rightfun();
            else if (texts.text == str5)
                Rightfun();
            else if (texts.text == str6)
                Rightfun();
            else
                Fail();
        }

     

        public override void OnFingerSwipe(LeanFinger finger)
        {
            // Make sure the info text exists
            // if (Text2 != null)
            // {
            // Store the swipe delta in a temp variable
            var swipe = finger.SwipeScreenDelta;

            if (RandomTextGenerator.Instance.praveen == true)
            {

                if (leftb == true)
                {
                    if (swipe.x < -Mathf.Abs(swipe.y))
                    {
                        Debug.Log("left");

                        lefttext(RandomTextGenerator.Instance.inputText, "Left", "NotNotLeft", "NotNothing", "NotRight", "NotUp", "NotDown");
                    }
                }

                else if (rightb == true)
                {
                    if (swipe.x < -Mathf.Abs(swipe.y))
                    {
                        righttext(RandomTextGenerator.Instance.inputText2, "Left", "NotNotLeft", "NotNothing", "NotRight", "NotUp", "NotDown");
                     }
                }

            }

            if (RandomTextGenerator.Instance.praveen == true)
            {

                if (leftb == true)
                {
                    if (swipe.x > Mathf.Abs(swipe.y))
                    {
                        Debug.Log("right");


                        lefttext(RandomTextGenerator.Instance.inputText, "Right", "NotNotRight", "NotNothing", "NotLeft", "NotUp", "NotDown");
                    }
                }

                else if (rightb == true)
                {
                    if (swipe.x > Mathf.Abs(swipe.y))
                    {
                        righttext(RandomTextGenerator.Instance.inputText2, "Right", "NotNotRight", "NotNothing", "NotLeft", "NotUp", "NotDown");
                    }
                }

            }


            if (RandomTextGenerator.Instance.praveen == true)
            {


                if (leftb == true)
                {
                    if (swipe.y < -Mathf.Abs(swipe.x))
                    {
                        Debug.Log("down");
                        lefttext(RandomTextGenerator.Instance.inputText, "Down", "NotNotDown", "NotNothing", "NotLeft", "NotUp", "NotRight");
                    }
                }

                else if (rightb == true)
                {
                    if (swipe.y < -Mathf.Abs(swipe.x))
                    {
                        righttext(RandomTextGenerator.Instance.inputText2, "Down", "NotNotDown", "NotNothing", "NotLeft", "NotUp", "NotRight");
                    }
                }

            }




            if (RandomTextGenerator.Instance.praveen == true)
            {

                    Debug.Log("up");

                if (RandomTextGenerator.Instance.inputText.text == "SWIPE UP")
                {
                    if (swipe.y > Mathf.Abs(swipe.x))
                    {
                        left1 = false;
                        right1 = false;
                        leftb = false;
                        rightb = false;
                        RandomTextGenerator.Instance.InputMessage();
                        RandomTextGenerator.Instance.Inputmessage2();
                        Time.timeScale = 1;
                    }
                }

                else if (leftb == true)
                {
                    if (swipe.y > Mathf.Abs(swipe.x))
                    {
                        lefttext(RandomTextGenerator.Instance.inputText, "Up", "NotNotUp", "NotNothing", "NotLeft", "NotRight", "NotDown");
                    }
                }
                else if (rightb == true)
                {
                    if (swipe.y > Mathf.Abs(swipe.x))
                    {
                        righttext(RandomTextGenerator.Instance.inputText2, "Up", "NotNotUp", "NotNothing", "NotLeft", "NotRight", "NotDown");
                    }
                }
         
                
            }

        }
    }
}






