﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using System.Collections;
using Khadga.FreakSpace;


namespace Lean.Touch
{

    // This script will tell you which direction you swiped in
    public class Level3 : LeanSwipeDirection4
    {
        Color[] colours = new Color[4];
   
        //[Tooltip("The text element we will display the swipe information in")]


        // Game Instance Singleton

        public override void Start()
        {
            Color greenclr = new Color(0, 204, 0, 255);
            colours[0] = Color.blue;
            colours[1] = Color.red;
            colours[2] = greenclr;
            colours[3] = Color.blue;
            RandomTextGenerator.Instance.starting();
        }


        public override void Success()
        {
            SoundManager.Instance.PlaySound("swipe");
            ScoreManager.scorevalue += 1;
            Time.timeScale = 1;
            Timer.time += 1;
            RandomTextGenerator.Instance.InputMessage();
            SPAWNER.Instance.spawn();
            if (ScoreManager.scorevalue > 15)
                RandomTextGenerator.Instance.inputText.color = colours[Random.Range(0, 4)];

        }

     
  
      void ColourMatch(Image dir)
        {
            if (dir.color == Color.red)
            {
                if (RandomTextGenerator.Instance.inputText.text == "RED" ||
                   RandomTextGenerator.Instance.inputText.text == "NotGREEN" ||
                   RandomTextGenerator.Instance.inputText.text == "NotBLUE" ||
                   RandomTextGenerator.Instance.inputText.text == "NotYELLOW")
                {
                    Success();
                }
                else
                    Fail();
            }


           else if (dir.color == Color.blue)
            {
                if (RandomTextGenerator.Instance.inputText.text == "BLUE" ||
                   RandomTextGenerator.Instance.inputText.text == "NotGREEN" ||
                   RandomTextGenerator.Instance.inputText.text == "NotRED" ||
                   RandomTextGenerator.Instance.inputText.text == "NotYELLOW")
                {
                    Success();
                }
                else
                    Fail();
            }

         else if (dir.color ==Color.green)
            {
                if (RandomTextGenerator.Instance.inputText.text == "GREEN" ||
                   RandomTextGenerator.Instance.inputText.text == "NotRED" ||
                   RandomTextGenerator.Instance.inputText.text == "NotBLUE" ||
                   RandomTextGenerator.Instance.inputText.text == "NotYELLOW")
                {
                    Success();
                }
                else
                    Fail();
            }

          else  if (dir.color == Color.yellow)
            {
                if (RandomTextGenerator.Instance.inputText.text == "YELLOW" ||
                   RandomTextGenerator.Instance.inputText.text == "NotGREEN" ||
                   RandomTextGenerator.Instance.inputText.text == "NotBLUE" ||
                   RandomTextGenerator.Instance.inputText.text == "NotRED")
                {
                    Success();
                }
                else
                    Fail();
            }

        }

        public override void OnFingerSwipe(LeanFinger finger)
        {
            // Make sure the info text exists
            // if (Text2 != null)
            // {
            // Store the swipe delta in a temp variable
            var swipe = finger.SwipeScreenDelta;

            if (RandomTextGenerator.Instance.praveen == true)
            {
                if (swipe.x < -Mathf.Abs(swipe.y))
                {
                    Debug.Log("left");
                    ColourMatch(SPAWNER.Instance.left);
                }

              
            }

            if (RandomTextGenerator.Instance.praveen == true)
            {

                if (swipe.x > Mathf.Abs(swipe.y))
                {
                    Debug.Log("right");
                    ColourMatch(SPAWNER.Instance.right);
                }

            }


            if (RandomTextGenerator.Instance.praveen == true)
            {

                if (swipe.y < -Mathf.Abs(swipe.x))
                {
                    Debug.Log("down");
                    ColourMatch(SPAWNER.Instance.down);
                }

            }


            if (RandomTextGenerator.Instance.praveen == true)
            {

                if (swipe.y > Mathf.Abs(swipe.x))
                {
                    Debug.Log("up");

                    if (RandomTextGenerator.Instance.inputText.text == "SWIPE UP")
                    {
                        Success();
                    }
                    else
                        ColourMatch(SPAWNER.Instance.up);


                }
            }

        }
    }
}







