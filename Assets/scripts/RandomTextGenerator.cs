﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Khadga.FreakSpace;

namespace Lean.Touch
{

    public class RandomTextGenerator : MonoBehaviour
    {
       
       
        public bool praveen = false;
        public GameObject Menubutton,GameOver_panel;
        public GameObject replaybutton;
        public Text inputText;
        public Text inputText2;
        public Text inputText3;
        public Text GameOver;
        private static RandomTextGenerator instance = null;
      


        // Game Instance Singleton
        public static RandomTextGenerator Instance
        {
            get
            {
                return instance;
            }
        }

        public void Awake()
        {
            Time.timeScale = 0;
            if (instance != null && instance != this)
            {
                // Destroy(this.gameObject);
            }
            instance = this;
            DontDestroyOnLoad(this.gameObject);
        }

  
        public void Start()
        {
            GameOver_panel.SetActive(false);
        }

        public void starting()
        {
            inputText.text = "SWIPE UP";
            praveen = true;
            ScoreManager.Instance.Score.enabled = true;
            Timer.Instance.timetext.enabled = true;
        }


        public void InputMessage()
        {

            Commands[] validCommands = new[] { Commands.NotNotUp, Commands.NotNotRight, Commands.NotNotLeft,
            Commands.NotNotDown,Commands.NotNotNothing, Commands.NotLeft,Commands.NotRight,
            Commands.NotUp,Commands.NotDown,Commands.NotNothing,Commands.Nothing,
            Commands.Down,Commands.Left,Commands.Right,Commands.Up,Commands.RED,
            Commands.BLUE,Commands.YELLOW,Commands.GREEN, Commands.NotRED,Commands.NotBLUE,Commands.NotYELLOW,Commands.NotGREEN };

            if (Levelmanager.Instance.Level_Index == 1)
            {
                Commands inputCommand = validCommands[Random.Range(10, validCommands.Length - 8)];
                Debug.Log(inputCommand);
                inputText.text = inputCommand.ToString();
            }
           else if (Levelmanager.Instance.Level_Index == 2)
            {
                Commands inputCommand = validCommands[Random.Range(0, validCommands.Length - 8)];
                Debug.Log(inputCommand);
                inputText.text = inputCommand.ToString();
            }

           else if (Levelmanager.Instance.Level_Index == 3)
            {
                if (ScoreManager.scorevalue  <= 15)
                {
                    Commands inputCommand = validCommands[Random.Range(15, validCommands.Length - 4)];
                    Debug.Log(inputCommand);
                    inputText.text = inputCommand.ToString();
                }
                else
                {
                    Commands inputCommand = validCommands[Random.Range(15, validCommands.Length - 1)];
                    Debug.Log(inputCommand);
                    inputText.text = inputCommand.ToString();
                }
            }

           else if (Levelmanager.Instance.Level_Index == 4||Levelmanager.Instance.Level_Index==5)
            {
                if (ScoreManager.scorevalue <= 15)
                {
                    Commands inputCommand = validCommands[Random.Range(9, validCommands.Length - 8)];
                    Debug.Log(inputCommand);
                    inputText.text = inputCommand.ToString();
                }
                else
                {

                    Commands inputCommand = validCommands[Random.Range(0, validCommands.Length - 8)];
                    Debug.Log(inputCommand);
                    inputText.text = inputCommand.ToString();
                }
            }
        }

        public void Inputmessage2()
        {
            Commands[] validCommands = new[] { Commands.Up, Commands.Right, Commands.Left,
            Commands.Down,Commands.Nothing, Commands.NotLeft,Commands.NotRight,
            Commands.NotUp,Commands.NotDown,Commands.NotNothing,Commands.NotNotNothing,
            Commands.NotNotDown,Commands.NotNotLeft,Commands.NotNotRight,Commands.NotNotUp};

            if (ScoreManager.scorevalue <= 15)
            {
                Commands inputCommand = validCommands[Random.Range(0, validCommands.Length - 11)];
                Debug.Log(inputCommand);
                inputText2.text = inputCommand.ToString();
            }
            else
            {
                Commands inputCommand = validCommands[Random.Range(0, validCommands.Length - 1)];
                Debug.Log(inputCommand);
                inputText2.text = inputCommand.ToString();
            }
        }

        public void Inputmessage3()
        {
            Commands[] validCommands = new[] { Commands.Up, Commands.Right, Commands.Left,
            Commands.Down,Commands.Nothing,Commands.NotLeft,Commands.NotRight,
            Commands.NotUp,Commands.NotDown,Commands.NotNothing,Commands.NotNotNothing,
            Commands.NotNotDown,Commands.NotNotLeft,Commands.NotNotRight,Commands.NotNotUp};

            if (ScoreManager.scorevalue<=15)
            {
                Commands inputCommand = validCommands[Random.Range(0, validCommands.Length - 11)];
                Debug.Log(inputCommand);
                inputText3.text = inputCommand.ToString();
            }
            else
            {
                Commands inputCommand = validCommands[Random.Range(0, validCommands.Length - 1)];
                Debug.Log(inputCommand);
                inputText3.text = inputCommand.ToString();
            }
        }



        public void Restartgame()
        {
            if (Levelmanager.Instance.Level_Index == 1)
                PlayerManager.Instance.AddCurrency("Level1",ScoreManager.scorevalue);
            if (Levelmanager.Instance.Level_Index == 2)
                PlayerManager.Instance.AddCurrency("Level2", ScoreManager.scorevalue);
            if (Levelmanager.Instance.Level_Index == 3)
                PlayerManager.Instance.AddCurrency("Level3", ScoreManager.scorevalue);
            if (Levelmanager.Instance.Level_Index == 4)
                PlayerManager.Instance.AddCurrency("Level4", ScoreManager.scorevalue);
            if (Levelmanager.Instance.Level_Index == 5)
                PlayerManager.Instance.AddCurrency("Level5", ScoreManager.scorevalue);

            praveen = true;
            Application.LoadLevel(Application.loadedLevel);
            ScoreManager.scorevalue = 0;
           
        }
     
        public void About()
        {
            Menubutton.SetActive(true);
        }

        public void back()
        {
            Screen.orientation = ScreenOrientation.Portrait;
            SceneManager.LoadScene("MAINMENU");
        }
       
    }
}