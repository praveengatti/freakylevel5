﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using System.Collections;
using Khadga.FreakSpace;

namespace Lean.Touch
{

    // This script will tell you which direction you swiped in
    public class Level4 : LeanSwipeDirection4
    {
     
        public GameObject colourselect;
        public int scam;
        public int colour;

        //  [Tooltip("The text element we will display the swipe information in")]

        public override void Start()
        {
            colourselect.SetActive(true);
        }

        public void colourmatch(Color clr,string str,string str2,string str3,string str4,string str5,string str6)
         {
            if (RandomTextGenerator.Instance.inputText.color == clr)
            {
                if (RandomTextGenerator.Instance.inputText.text == str)
                    Success();
                else if (RandomTextGenerator.Instance.inputText.text == str2)
                    Success();
                else if (RandomTextGenerator.Instance.inputText.text == str3)
                    Success();
                else if (RandomTextGenerator.Instance.inputText.text == str4)
                    Success();
                else if (RandomTextGenerator.Instance.inputText.text == str5)
                    Success();
                else if (RandomTextGenerator.Instance.inputText.text == str6)
                    Success();
                else
                    Fail();
            }
            else if (RandomTextGenerator.Instance.inputText2.color == clr)
            {
                if (RandomTextGenerator.Instance.inputText2.text == str)
                    Success();
                else if (RandomTextGenerator.Instance.inputText2.text == str2)
                    Success();
                else if (RandomTextGenerator.Instance.inputText2.text == str3)
                    Success();
                else if (RandomTextGenerator.Instance.inputText2.text == str4)
                    Success();
                else if (RandomTextGenerator.Instance.inputText2.text == str5)
                    Success();
                else if (RandomTextGenerator.Instance.inputText2.text == str6)
                    Success();
                else
                    Fail();
            }
            else if (RandomTextGenerator.Instance.inputText3.color ==clr)
            {
                if (RandomTextGenerator.Instance.inputText3.text == str)
                    Success();
                else if (RandomTextGenerator.Instance.inputText3.text == str2)
                    Success();
                else if (RandomTextGenerator.Instance.inputText3.text == str3)
                    Success();
                else if (RandomTextGenerator.Instance.inputText3.text == str4)
                    Success();
                else if (RandomTextGenerator.Instance.inputText3.text == str5)
                    Success();
                else if (RandomTextGenerator.Instance.inputText3.text == str6)
                    Success();
                else
                    Fail();
            }
        }

        public void red()
        {
            colour = 1;
            RandomTextGenerator.Instance.starting();
            colourselect.SetActive(false);
        }
        public void blue()
        {
            colour = 2;
            RandomTextGenerator.Instance.starting();
            colourselect.SetActive(false);
        }
        public void green()
        {
            colour = 3;
            RandomTextGenerator.Instance.starting();
            colourselect.SetActive(false);
        }

        public void impartcolor(Text t1, Text t2, Text t3)
        {
            t1.color = Color.red;
            t2.color = Color.blue;
            t3.color = Color.green;
        }

        public void impartingcolour()
        {
            scam = Random.Range(0, 5);
            if (scam == 0)
                impartcolor(RandomTextGenerator.Instance.inputText, RandomTextGenerator.Instance.inputText2, RandomTextGenerator.Instance.inputText3);
            if (scam == 1)
                impartcolor(RandomTextGenerator.Instance.inputText2, RandomTextGenerator.Instance.inputText, RandomTextGenerator.Instance.inputText3);
            if (scam == 2)
                impartcolor(RandomTextGenerator.Instance.inputText2, RandomTextGenerator.Instance.inputText3, RandomTextGenerator.Instance.inputText);
            if (scam == 3)
                impartcolor(RandomTextGenerator.Instance.inputText, RandomTextGenerator.Instance.inputText3, RandomTextGenerator.Instance.inputText2);
            if (scam == 4)
                impartcolor(RandomTextGenerator.Instance.inputText3, RandomTextGenerator.Instance.inputText2, RandomTextGenerator.Instance.inputText);
            if (scam == 5)
                impartcolor(RandomTextGenerator.Instance.inputText3, RandomTextGenerator.Instance.inputText, RandomTextGenerator.Instance.inputText2);
        }




        public new void Success()
        {
            SoundManager.Instance.PlaySound("swipe");
            impartingcolour();
            RandomTextGenerator.Instance.InputMessage();
            RandomTextGenerator.Instance.Inputmessage2();
            RandomTextGenerator.Instance.Inputmessage3();
            Timer.time += 1;
            Time.timeScale = 1;
            ScoreManager.scorevalue += 1;
        }

     


        public override void OnFingerSwipe(LeanFinger finger)
        {
            // Make sure the info text exists
            // if (Text2 != null)
            // {
            // Store the swipe delta in a temp variable
            var swipe = finger.SwipeScreenDelta;

            if (RandomTextGenerator.Instance.praveen == true)
            {
                if (swipe.x < -Mathf.Abs(swipe.y))
                {
                    Debug.Log("left");
                    if(colour==1)
                    {
                        colourmatch(Color.red, "Left","NotNothing","NotNotLeft","NotRight","NotUp","NotDown");
                    }

                  else  if (colour == 2)
                    {
                        colourmatch(Color.blue, "Left", "NotNothing", "NotNotLeft", "NotRight", "NotUp", "NotDown");
                    }
                   else if (colour == 3)
                    {
                        colourmatch(Color.green, "Left", "NotNothing", "NotNotLeft", "NotRight", "NotUp", "NotDown");
                    }

                    else
                    {
                        Fail();
                    }

                }

            }

            if (RandomTextGenerator.Instance.praveen == true)
            {

                if (swipe.x > Mathf.Abs(swipe.y))
                {
                    Debug.Log("right");
                    if (colour == 1)
                    {
                        colourmatch(Color.red, "Right", "NotNothing", "NotNotRight", "NotLeft", "NotUp", "NotDown");
                    }

                    else if (colour == 2)
                    {
                        colourmatch(Color.blue, "Right", "NotNothing", "NotNotRight", "NotLeft", "NotUp", "NotDown");
                    }
                    else if (colour == 3)
                    {
                        colourmatch(Color.green, "Right", "NotNothing", "NotNotRight", "NotLeft", "NotUp", "NotDown");
                    }

                    else
                    {
                        Fail();
                    }

                }

            }


            if (RandomTextGenerator.Instance.praveen == true)
            {

                if (swipe.y < -Mathf.Abs(swipe.x))
                {
                    Debug.Log("down");

                    if (colour == 1)
                    {
                        colourmatch(Color.red, "Down", "NotNothing", "NotNotDown", "NotRight", "NotUp", "NotLeft");
                    }

                    else if (colour == 2)
                    {
                        colourmatch(Color.blue, "Down", "NotNothing", "NotNotDown", "NotRight", "NotUp", "NotLeft");
                    }
                    else if (colour == 3)
                    {
                        colourmatch(Color.green, "Down", "NotNothing", "NotNotDown", "NotRight", "NotUp", "NotLeft");
                    }

                    else
                    {
                        Fail();
                    }

                }

            }


            if (RandomTextGenerator.Instance.praveen == true)
            {

                if (swipe.y > Mathf.Abs(swipe.x))
                {
                    Debug.Log("up");

                    if (RandomTextGenerator.Instance.inputText.text == "SWIPE UP")
                    {
                            Success();
                    }

                   else if (colour == 1)
                    {
                        colourmatch(Color.red, "Up", "NotNothing", "NotNotUp", "NotRight", "NotLeft", "NotDown");
                    }

                    else if (colour == 2)
                    {
                        colourmatch(Color.blue, "Up", "NotNothing", "NotNotUp", "NotRight", "NotLeft", "NotDown");
                    }
                    else if (colour == 3)
                    {
                        colourmatch(Color.green, "Up", "NotNothing", "NotNotUp", "NotRight", "NotLeft", "NotDown");
                    }


                    else
                    {
                        Fail();
                    }

                }
            }

        }
    }
}






