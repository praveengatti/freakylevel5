﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;
using UnityEngine.UI;

public enum Commands
{
    [Description("Right")]
    Right,
    [Description("Left")]
    Left,
    [Description("Up")]
    Up,
    [Description("Down")]
    Down,
    [Description("Not Right")]
    NotRight,
    [Description("Not Left")]
    NotLeft,
    [Description("Not Up")]
    NotUp,
    [Description("Not Down")]
    NotDown,
    [Description("Nothing")]
    Nothing,
    [Description("Not Nothing")]
    NotNothing,
    [Description("Not Not Nothing")]
    NotNotNothing,
    [Description("Not Not Right")]
    NotNotRight,
    [Description("Not Not Left")]
    NotNotLeft,
    [Description("Not Not Up")]
    NotNotUp,
    [Description("Not Not Down")]
    NotNotDown,
    [Description("RED")]
     RED,
    [Description("BLUE")]
     BLUE,
    [Description("YELLOW")]
    YELLOW,
    [Description("GREEN")]
    GREEN,
    [Description("Not RED")]
    NotRED,
    [Description("Not BLUE")]
    NotBLUE,
    [Description("Not YELLOW")]
    NotYELLOW,
    [Description("Not GREEN")]
    NotGREEN
}


