﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Khadga.FreakSpace;
using UnityEngine.UI;

namespace Lean.Touch
{
    public class Initial : LeanSwipeDirection4
    {
        public GameObject TapButton;
        public GameObject Mainpanel;
        public GameObject settingspanel;
        public Text popup;
        public bool enable=false;

       public override void Start()
        {
            settingspanel.SetActive(false);
            Mainpanel.SetActive(false);
        }

        public void AppearMenu()
        {
            Mainpanel.SetActive(true);
            enable = true;
        }

        public void DisapperMenu()
        {
            Mainpanel.SetActive(false);
        }

    
        public void back()
        {
            SceneManager.LoadScene("START");
        }
        public override void OnFingerSwipe(LeanFinger finger)
        {
            // Make sure the info text exists
            // if (Text2 != null)
            // {
            // Store the swipe delta in a temp variable
            var swipe = finger.SwipeScreenDelta;
            if (enable == true)
            {

                if (swipe.x < -Mathf.Abs(swipe.y))
                {
                    Debug.Log("Left");
                    Mainpanel.SetActive(false);
                    settingspanel.SetActive(true);
                    TapButton.SetActive(false);
                }


                if (swipe.x > Mathf.Abs(swipe.y))
                {
                    Debug.Log("right");

                    SceneManager.LoadScene("MAINMENU");
                    // RandomTextGenerator.Instance.praveen = true;
                }


                if (swipe.y < -Mathf.Abs(swipe.x))
                {
                    Debug.Log("down");


                }


                if (swipe.y > Mathf.Abs(swipe.x))
                {

                }

            }

            }
        }
    }