using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using System.Collections;
using Khadga.FreakSpace;

namespace Lean.Touch
{

    // This script will tell you which direction you swiped in
    public class LeanSwipeDirection4 : MonoBehaviour
    {
   

        [Tooltip("The text element we will display the swipe information in")]
        public bool left1, right1 = false;
        public static LeanSwipeDirection4 Instance;
        

        // Game Instance Singleton
        //public static LeanSwipeDirection4 Instance
        //{
        //    get
        //    {
        //        return instance;
        //    }
        //}

        public void Awake()
        {
            Time.timeScale = 0;
            if (Instance != null && Instance != this)
            {
                // Destroy(this.gameObject);
            }

            Instance = this;
            //DontDestroyOnLoad(this.gameObject);
        }

        public virtual void Start()
        {

        }

        public virtual void Success()
          {
            Time.timeScale = 1;
            Timer.time += 1;
            
        }

        public virtual void Fail()
        {
            RandomTextGenerator.Instance.GameOver_panel.SetActive(true);
            Update_Score();
            SoundManager.Instance.PlaySound("gameover");
            RandomTextGenerator.Instance.praveen = false;
            Time.timeScale = 0;
            RandomTextGenerator.Instance.inputText.enabled = false;
            RandomTextGenerator.Instance.inputText2.enabled = false;
            RandomTextGenerator.Instance.inputText3.enabled = false;
            ScoreManager.Instance.Score.enabled = false;
            Timer.Instance.timetext.enabled = false;
            Timer.time = 0;
        }
        
        void Update_Score()
        {
            if(Levelmanager.Instance.Level_Index==1)
                PlayerManager.Instance.AddCurrency("Level1", ScoreManager.scorevalue);
           else if (Levelmanager.Instance.Level_Index == 2)
                PlayerManager.Instance.AddCurrency("Level2", ScoreManager.scorevalue);
           else if (Levelmanager.Instance.Level_Index == 3)
                PlayerManager.Instance.AddCurrency("Level3", ScoreManager.scorevalue);
           else if (Levelmanager.Instance.Level_Index == 4)
                PlayerManager.Instance.AddCurrency("Level4", ScoreManager.scorevalue);
           else if (Levelmanager.Instance.Level_Index == 5)
                PlayerManager.Instance.AddCurrency("Level5", ScoreManager.scorevalue);
        }

        protected virtual void OnEnable()
        {

            // Hook into the events we need
            LeanTouch.OnFingerSwipe += OnFingerSwipe;

        }

        protected virtual void OnDisable()
        {
            // Unhook the events
            LeanTouch.OnFingerSwipe -= OnFingerSwipe;
        }

        public virtual void OnFingerSwipe(LeanFinger finger)
        {
            // Make sure the info text exists
            // if (Text2 != null)
            // {
            // Store the swipe delta in a temp variable
            var swipe = finger.SwipeScreenDelta;

                if (swipe.x < -Mathf.Abs(swipe.y))
                {
                    Debug.Log("left");
                    
                }


       

                if (swipe.x > Mathf.Abs(swipe.y))
                {
                    Debug.Log("right");

                }

      
                if (swipe.y < -Mathf.Abs(swipe.x))
                {
                    Debug.Log("down");
                }

          

                if (swipe.y > Mathf.Abs(swipe.x))
                {
                    Debug.Log("up");

                    //if (RandomTextGenerator.Instance.inputText.text == "SWIPE UP")
                    //{
                    //    if (Levelmanager.Instance.cdef != 4)
                    //    {
                    //        Freaky.Instance.leftb = false;
                    //        Freaky.Instance.rightb = false;
                    //        RandomTextGenerator.Instance.InputMessage();
                    //        RandomTextGenerator.Instance.Inputmessage2();
                    //        Time.timeScale = 1;
                    //        ScoreManager.Instance.ScoreValue += 1;
                    //        Timer.time += 1;
                    //        if (Levelmanager.Instance.cdef == 3)
                    //        {
                    //            SPAWNER.Instance.spawn();
                    //        }
                    //    }
                    //    else
                    //        Success();
                    //}
                }
            }

        }
    }






