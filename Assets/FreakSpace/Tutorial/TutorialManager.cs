﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Khadga.FreakSpace
{
    public class TutorialManager : MonoBehaviour
    {
        [Tooltip("sorting order of the tutorial images in the canvas")]
        public int SortingOrder = 50;
        private Vector2 Resolution; //temporary variable for storing the resolution of the screen
        [Tooltip("sprites to be shown in tutorial in order")]
        public List<Sprite> TutorialImages;
        private GameObject ParentCanvas;//temporary canvas needed for ui-images
        private GameObject TutorialImage;//the ui-image in which sprites change in order
        private int TutorialImageCount = 0;//variable which indicates the index of the sprite that is being shown in tutorial images

        /// <summary>
        /// 
        /// </summary>
        private void Start()
        {
            //Setting resolution to current screen width and height
            Resolution = new Vector2(Screen.currentResolution.width, Screen.currentResolution.height);
        }

        /// <summary>
        /// Starts tutorial over current UI
        /// </summary>
        public void StartTutorial()
        {
            if (TutorialImageCount == 0)
                InitializeTutorialCanvas();
            if (TutorialImageCount != TutorialImages.Count)
                TutorialImage.GetComponent<Image>().sprite = TutorialImages[TutorialImageCount++];
            else
                CompleteTutorial();
        }

        /// <summary>
        /// Completes tutorial, closes all tutorial popups
        /// </summary>
        public void CompleteTutorial()
        {
            //Destryoying the tutorial parent
            TutorialImageCount = 0;
            Destroy(ParentCanvas);
        }

        /// <summary>
        /// Initializes the tutorial canvas
        /// </summary>
        private void InitializeTutorialCanvas()
        {
            // Creating a canvas and intiializing properties
            //initialising by making imagecount zero
            TutorialImageCount = 0;
            //creation of new canvas in the scene
            ParentCanvas = new GameObject("NewerCanvas");
            ParentCanvas.AddComponent<RectTransform>();
            ParentCanvas.AddComponent<Canvas>();
            ParentCanvas.AddComponent<CanvasScaler>();
            ParentCanvas.AddComponent<GraphicRaycaster>();
            ParentCanvas.layer = 5;
            ParentCanvas.GetComponent<Canvas>().renderMode = 0;
            ParentCanvas.GetComponent<Canvas>().sortingOrder = SortingOrder;
            //creation of new button in the scene in which images will be swapped
            TutorialImage = new GameObject("TutorialImage");
            TutorialImage.transform.SetParent(ParentCanvas.transform);
            TutorialImage.AddComponent<RectTransform>();
            TutorialImage.layer = 5;
            TutorialImage.AddComponent<Image>();
            TutorialImage.AddComponent<Button>();
            TutorialImage.GetComponent<RectTransform>().sizeDelta = Resolution;
            TutorialImage.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 0);
            //creating a temporary variable for giving onclick method for the tutuorial image
            Button b = TutorialImage.GetComponent<Button>();
            b.onClick.AddListener(OnClick);

        }

        private void OnClick()
        {
            StartTutorial();
        }
    }
}