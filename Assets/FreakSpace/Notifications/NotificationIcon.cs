﻿namespace Khadga.FreakSpace
{
    public enum NotificationIcon
    {
        Bell,
        Clock,
        Event,
        Heart,
        Message,
        Star
    }
}