﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using Lean.Touch;

namespace Khadga.FreakSpace
{

    public class PlayerManager : MonoBehaviour
    {


        public static PlayerManager Instance;
        [System.Serializable]
        public class Currency
        {
            public string Name;
            internal int value;
            public int DefaultValue;
        }

        public List<Currency> Currencies;

        int Score { get; set; }
        int HighScore { get; set; }
        int InitialScore = 0;
        const string SCORE = "SCORE";   // key name to score in PlayerPrefs
        const string HIGHSCORE = "HIGHSCORE";   // key name to high score in PlayerPrefs


        void Awake()
        {

            //if (Instance)
            //{
            //   // DestroyImmediate(gameObject);
            //}
            //else
            //{
            //    Instance = this;
            //    DontDestroyOnLoad(gameObject);
            //}
            Instance = this;
            DontDestroyOnLoad(gameObject);

            Initialize();

        }

        // Initialize currency and score
        public void Initialize()
        {
            foreach (var currency in Currencies)
            {
                currency.value = PlayerPrefs.GetInt(currency.Name, currency.DefaultValue);
            }
            Score = PlayerPrefs.GetInt(SCORE, InitialScore);
            HighScore = PlayerPrefs.GetInt(HIGHSCORE, 0);
        }

        //Update Score
        public void UpdateScore(int score)
        {
            Score = score;
            PlayerPrefs.SetInt(SCORE, Score);
            if (HighScore < Score)
            {
                HighScore = Score;
                PlayerPrefs.SetInt(HIGHSCORE, HighScore);
            }
        }

        public int GetScore()
        {
            return Score;
        }

        public int GetHighScore()
        {
            return HighScore;
        }

        /// <summary>
        /// returns amount of currency having particular name
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public int GetCurrency(string name)
        {
            int x = GetCurrencyWithName(name).value;
            return x;
        }

        /// <summary>
        /// Adds amount of currency to particular name
        /// </summary>
        /// <param name="name"></param>
        /// <param name="amount"></param>
       
        public void AddCurrency(string name, int amount)
        {
            if (GetCurrencyWithName(name).value < amount)
            {
                Debug.Log(name + ", is increased by: " + amount);

                // Store new currency value
                GetCurrencyWithName(name).value = amount;
                PlayerPrefs.SetInt(name, amount);
            }
         
        }

        public void ConsumeCurrency(string name, int amount)
        {
            GetCurrencyWithName(name).value -= amount;

            Debug.Log(name+" is decreased by: " + amount);

            // Store new coin value
            PlayerPrefs.SetInt(name, GetCurrencyWithName(name).value);

        }

        Currency GetCurrencyWithName(string name)
        {
            return Currencies.Find(c => c.Name == name);
        }
    }
}