﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine.SocialPlatforms;

namespace Khadga.FreakSpace
{
    public static class GooglePlayWrapper
    {
        
        /// <summary>
        /// Call this method before any call to google play services
        /// </summary>
        public static void InitializeServices()
        {
            Social.localUser.Authenticate((bool success) => {
                // handle success or failure
            });

        }

        /// <summary>
        /// Show all leaderboards available for the game
        /// </summary>
        public static void ShowLeaderboard()
        {
            Social.ShowLeaderboardUI();
        }

        /// <summary>
        /// Show only single leaderboard with ID
        /// </summary>
        /// <param name="ID"></param>
        public static void ShowLeaderboard(string ID)
        {
            Debug.LogError("Unique leaderboard code Not implemented");
            //Naveen:  calling all leaderboards instead of single. revert this to unique leaderboard whne needed
            ShowLeaderboard();
        }


        /// <summary>
        /// Post score to respective leaderboard id
        /// </summary>
        /// <param name="score"></param>
        /// <param name="leaderboardID"></param>
        public static void PostScoreToLeaderboard(long score, string leaderboardID)
        {
            Social.ReportScore(score, leaderboardID, (bool success) => {
                // handle success or failure
                Debug.Log("leaderboard posting status: " + success);
            });

        }

    }
}