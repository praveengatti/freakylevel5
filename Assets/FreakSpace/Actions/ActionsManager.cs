﻿using UnityEngine;
using System.Collections;
using GooglePlayGames;
using System;
using UnityEngine.Advertisements;
using UnityEngine.UI;

namespace Khadga.FreakSpace
{

    // Class containing all actions share, rate, view video ad, Show leaderboard
    public class ActionsManager : MonoBehaviour
    {
        public static ActionsManager Instance;

        [Tooltip("The play store link")]
        public string PlayStoreShareLink;

        [Tooltip("The AppStore share link")]
        public string AppstoreShareLink;

        [Tooltip("The Message which is to be displayed when shared")]
        public string ShareMessage;

        [Tooltip("Leaderboard ID in google developer console")]
        public string LeaderboardID;

        

        void Awake()
        {

            if (Instance)
            {
                DestroyImmediate(gameObject);
            }
            else
            {
                Instance = this;
                DontDestroyOnLoad(gameObject);
            }

            GooglePlayWrapper.InitializeServices();

        }

        public void AndroidShare()
        {
            Debug.Log("Share icon clicked");
            StartCoroutine(ShareAndroidText());

        }

        IEnumerator ShareAndroidText()
        {
            yield return new WaitForEndOfFrame();
#if UNITY_ANDROID
        AndroidJavaClass intentClass = new AndroidJavaClass("android.content.Intent");
        AndroidJavaObject intentObject = new AndroidJavaObject("android.content.Intent");
        intentObject.Call<AndroidJavaObject>("setAction", intentClass.GetStatic<string>("ACTION_SEND"));
        intentObject.Call<AndroidJavaObject>("setType", "text/plain");
        intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_SUBJECT"), ShareMessage);
      //  intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TEXT"), ShareLink);
        AndroidJavaClass unity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject currentActivity = unity.GetStatic<AndroidJavaObject>("currentActivity");
        AndroidJavaObject jChooser = intentClass.CallStatic<AndroidJavaObject>("createChooser", intentObject, "Share Via");
        currentActivity.Call("startActivity", jChooser);
#endif
        }

        public void RateUs()
        {
#if UNITY_ANDROID
		Application.OpenURL(PlayStoreShareLink);
#elif UNITY_IPHONE
		Application.OpenURL(AppstoreShareLink);
#endif
        }

        public void ShowGooglePlayLeaderboard()
        {
            GooglePlayWrapper.ShowLeaderboard();
        }

        public void PostScoreToGoogleLeaderboard(int score)
        {
            GooglePlayWrapper.PostScoreToLeaderboard(score,LeaderboardID);
        }
    }
}